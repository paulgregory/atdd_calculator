﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace ATDD_Calculator
{
    [Binding]
    public class CalculatorSteps
    {
        private int _result;
        private Table PairArguments;

        [When(@"I add (.*) and (.*)")]
        public void WhenIAddAnd(int p0, int p1)
        {
            _result = Calculator.Add(p0, p1);
    
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            Assert.AreEqual(p0, _result);
        }

        [Given(@"the following argument pairs:")]
        public void GivenTheFollowingArgumentPairs(Table table)
        {
     
        }

        [Then(@"I should get the following list of results:")]
        public void ThenIShouldGetTheFollowingListOfResults(Table table)
        {
            ScenarioContext.Current.Pending();
        }

    
    }

    public class Argument
    {
        public int Argument1;
        public int Argument2;
    }
	
}
